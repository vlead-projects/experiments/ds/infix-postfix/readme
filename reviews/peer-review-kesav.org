#+TITLE: Review of Tree Traversal Experiment
#+AUTHOR: VLEAD
#+DATE: [2019-06-24 Mon] 
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document captures the review comments for the
  =Infix to Postfix= experiment.
  
* Review Comments
  + Please find the review comments for an experiment. Once
  the issue is fixed, update the status of an issue in the
  below table.  Also, comment and add label(eg:
  Resolved/Differed etc.,) to the Git issue to track the
  status.
  + Make sure you put descriptions of all the variables that are used in the Javascript. Description should say what the variable does and where it is used.
  + Add descriptions for each code block instead of clustering it all.
  |------+-----------+------------------------------------+---------------|
  | S.NO | Project   | Issues                             | Resolved(Y/N) and Comments |
  |------+-----------+------------------------------------+---------------|
  |   1. | Common Issues content  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/96][Issue 1]]  |      |
  |------+-----------+------------------------------------+---------------|
  |   2. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/89][Issue 2]]  |       |
  |------+-----------+------------------------------------+---------------|
  |   3. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/90][Issue 3]]  |       |
  |------+-----------+------------------------------------+---------------|
  |   4. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/91][Issue 4]]  |       |
  |------+-----------+------------------------------------+---------------|
  |   5. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/92][Issue 5]]  |       |
  |------+-----------+------------------------------------+---------------|
  |   6. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/93][Issue 5]]  |       |
  |------+-----------+------------------------------------+---------------|
  |   7. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/94][Issue 5]]  |       |
  |------+-----------+------------------------------------+---------------|
  |   8. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/95][Issue 5]]  |       |
  |------+-----------+------------------------------------+---------------|
  |   9. | Common Issues Artefacts  | [[https://gitlab.com/vlead-projects/experiments/ds/infix-to-postfix/artefacts/issues/97][Issue 5]]  |       |
  |------+-----------+------------------------------------+---------------|